# Computational Social Science Analysis

An analysis pursued as a part of the final assigment for _Collecting and Analysing Big Data for Social Sciences_ course, KU Leuven, academic year 2021-22. The utilised data was retrieved from https://www.kaggle.com/datasets/gpreda/all-covid19-vaccines-tweets .
